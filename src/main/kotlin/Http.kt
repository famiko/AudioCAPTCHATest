import MscTest.getMscObj
import com.iflytek.cloud.speech.SpeechUtility
import khttp.get
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.util.*

object Http {

    var date = SimpleDateFormat("yyyy-MM-dd-HH,mm,ss").format(Date())

    var logFile = File("$date\\log.txt")

    val url = "http://localhost"

    var audioDir = File("$date\\audio_files")

    var cookie: Map<String, String>? = null

    var fileName: String = ""

    var count = 0

    var success = 0
    init {
        if (!File(date).exists()) {
            File(date).mkdir()
        }
        if (!logFile.exists()) {
            logFile.createNewFile()
        }
        if (!audioDir.exists()) {
            audioDir.mkdir()
        }
    }

    fun log(string: String) {
        val str = string.removeSuffix("。")
        val result = validate(str)
        if (result) {
            success++
        }
        logFile.appendText("<${Date()}>第${++count}次测试，音频文件：$fileName，识别结果：$str，验证结果：$result\n")
    }

    fun getStream(): FileInputStream {

        val first = get("$url/audio")
        val nextURL = first.text
        cookie = first.cookies.toMap()
        val res = get("$url$nextURL")
        val file = File("${audioDir.absolutePath}\\${nextURL.substring(nextURL.lastIndexOf('/') + 1)}")
        fileName = file.name
        if (!file.exists()) {
            file.createNewFile()
        }
        file.writeBytes(res.content)
        return file.inputStream()
    }

    fun validate(string: String): Boolean {
        val code = get("$url/audio_check/$string", cookies = cookie).statusCode
        return code == 200
    }
}

fun main(args: Array<String>) {
    SpeechUtility.createUtility("appid=" + MscTest.APPID)
    (1..50).forEach {
        getMscObj().loop(Http.getStream())
        getMscObj().mIsLoop = true
        Thread.sleep(2000)
    }
    Thread.sleep(4000)
    Http.logFile.appendText("总计次数：${Http.count}，成功识别次数：${Http.success}，识别率：${Http.success.toDouble()/Http.count}")
}

