import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by dbfan on 2017/5/16.
 */
fun main(args: Array<String>) {
    var date = SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(Date())
    println(date)
}